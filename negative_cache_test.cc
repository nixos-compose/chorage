#include "negative_cache.h"

#include <cstdio>

int main() {

  printf("Is there an entry for (200, file200): %d\n",
         in_negative_cache(200, "file200"));
  insert_negative_cache(200, "file200", 4);
  printf("Is there an entry for (200, file200): %d\n",
         in_negative_cache(200, "file200"));
  int value;
  if ((value = get_negative_cache(200, "file200")) != 0)
    printf("Get entry (200, file200): %d\n", value);
  else
    printf("Get entry (200, file200): not in cache\n");
  if ((value = get_negative_cache(200, "file300")) != 0)
    printf("Get entry (200, file300): %d\n", value);
  else
    printf("Get entry (200, file300): not in cache\n");

  insert_negative_cache(300, "file300", 54);
  if ((value = get_negative_cache(300, "file200")) != 0)
    printf("Get entry (300, file200): %d\n", value);
  else
    printf("Get entry (300, file200): not in cache\n");
  if ((value = get_negative_cache(300, "file300")) != 0)
    printf("Get entry (300, file300): %d\n", value);
  else
    printf("Get entry (300, file300): not in cache\n");

  // update_negative_cache(200, "file200", 5);
  // if ((value = get_negative_cache(200, "file200")) != 0)
  //   printf("Get updated entry (200, file200): %d", value);
  // else
  //   printf("Get updated entry (200, file200): not in cache");

  print_negative_cache();

  return 0;
}