#include "cache_log.hpp"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <string>

CacheLog::CacheLog() {
  log.reserve(1000000);
}

void CacheLog::append(LogEntry new_entry) { log.push_back(new_entry); }

void CacheLog::set_file_path(std::string file_path) {
  output_file.open(file_path, std::ios::out | std::ios::trunc);
}

bool CacheLog::write_file() {
  if (!output_file.is_open())
    return false;

  std::ostream_iterator<std::string> output_iterator(output_file, "\n");
  std::transform(std::begin(log), std::end(log), output_iterator,
                 [](const CacheLog::LogEntry entry) {
                   return std::string((entry.status == CacheLogStatus::hit)
                                          ? "hit"
                                          : "miss") +
                          "," + entry.path + "," + entry.name;
                 });
  return true;
}

void CacheLog::print_csv() {
  std::cout << "cache_status, path, name" << std::endl;
  for (auto entry : log) {
    if (entry.status == CacheLogStatus::hit)
      std::cout << "hit"
                << "," << entry.path << "," << entry.name << std::endl;
    else {
      std::cout << "miss"
                << "," << entry.path << "," << entry.name << std::endl;
    }
  }
}