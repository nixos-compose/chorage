#ifndef TRACE_PLAYER_HPP
#define TRACE_PLAYER_HPP

#include "trace.hpp"
#include <string>
#include <utility>
#include <map>

class TracePlayer {
public:
  TracePlayer(std::string trace_path);

  std::pair<operation_t, std::string> parse_trace_line(std::string line);

  void do_operation(operation_t operation, std::string path);

private:
  std::string trace_path;
  std::map<std::string, DIR*> open_directories;
  std::map<std::string, int> open_files;
};
#endif