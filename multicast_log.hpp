#ifndef MULTICAST_LOG_H
#define MULTICAST_LOG_H

#include <fstream>
#include <string>
#include <unordered_map>
#include <vector>

/**
 * A log of all cache hits and misses, to be written to a file for performance
 * analysis
 */
class MulticastLog {
public:
  MulticastLog();

  void increment(std::string source);
  void set_file_path(std::string file_path);
  bool write_file();
  void print_csv();

private:
  std::unordered_map<std::string, int> receive_log;
  std::ofstream output_file;
};

#endif // MULTICAST_LOG_H