{ pkgs, ... }: {
  roles = {
    multicast_test = { pkgs, ... }:
      let chorage = pkgs.callPackage ../package.nix { };
      in
      {
        # add needed package
        environment.systemPackages = with pkgs; [
          chorage
          socat
        ];

        networking.firewall.enable = false;
      };
  };
  testScript = ''
    foo.succeed("true")
  '';
}
