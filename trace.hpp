#ifndef TRACE_HPP
#define TRACE_HPP

#include "chorage_inode.hpp"
#include <fstream>
#include <string>
#include <unordered_map>
#include <vector>

enum operation_t {
  op_getattr,
  op_lookup,
  op_readlink,
  op_opendir,
  op_readdir,
  op_readdirplus,
  op_releasedir,
  op_open,
  op_release,
  op_read,
  op_statfs,
  op_flock,
  op_getxattr,
  op_listxattr
};

class FuseTrace {
public:
  FuseTrace();

  void append(const operation_t operation, const Inode &inode);
  void write_trace_stream_to(std::string file_path);
  void write_file(std::string file_path);

  static const std::string operation_to_string(const operation_t operation);
  static const operation_t string_to_operation(const std::string operation);

  struct trace_entry {
    operation_t operation;
    std::string path;
  };

private:
  std::vector<trace_entry> trace;
  std::ofstream output_file; // where to write the trace as it is being created,
                             // rather all at once

  static const std::unordered_map<operation_t, std::string>
      operation_string_mapping;
  static const std::unordered_map<std::string, operation_t>
      string_operation_mapping;
};
#endif