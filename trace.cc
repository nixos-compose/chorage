#include "trace.hpp"
#include "settings.hpp"
#include <algorithm>
#include <fstream>
#include <iterator>
#include <string>
#include <unordered_map>
#include <vector>

FuseTrace::FuseTrace() {
  trace.reserve(10000);
}

void FuseTrace::append(const operation_t operation, const Inode &inode) {
#ifdef TRACING_ENABLE
  trace.push_back({.operation = operation, .path = inode.path});

  // Conditionally write to the trace file right away
  if (this->output_file.is_open()) {
    output_file << operation_to_string(operation) << ":" << inode.path
                << std::endl;
  }
#else
  (void)operation;
  (void)inode;
#endif
}

void FuseTrace::write_trace_stream_to(std::string file_path) {
  output_file.open(file_path);
}

std::string trace_to_string(FuseTrace::trace_entry entry) {
  return std::to_string(entry.operation) + ":" + entry.path;
}

void FuseTrace::write_file(std::string file_path) {
  std::ofstream output_file(file_path);

  std::ostream_iterator<std::string> output_iterator(output_file, "\n");
  std::transform(std::begin(trace), std::end(trace), output_iterator,
                 trace_to_string);
}

const std::unordered_map<operation_t, std::string>
    FuseTrace::operation_string_mapping = {
        {op_getattr, "op_getattr"},       {op_lookup, "op_lookup"},
        {op_readlink, "op_readlink"},     {op_opendir, "op_opendir"},
        {op_readdir, "op_readdir"},       {op_readdirplus, "op_readdirplus"},
        {op_releasedir, "op_releasedir"}, {op_open, "op_open"},
        {op_release, "op_release"},       {op_read, "op_read"},
        {op_statfs, "op_statfs"},         {op_flock, "op_flock"},
        {op_getxattr, "op_getxattr"},     {op_listxattr, "op_listxattr"}};

const std::unordered_map<std::string, operation_t>
    FuseTrace::string_operation_mapping = {
        {"op_getattr", op_getattr},       {"op_lookup", op_lookup},
        {"op_readlink", op_readlink},     {"op_opendir", op_opendir},
        {"op_readdir", op_readdir},       {"op_readdirplus", op_readdirplus},
        {"op_releasedir", op_releasedir}, {"op_open", op_open},
        {"op_release", op_release},       {"op_read", op_read},
        {"op_statfs", op_statfs},         {"op_flock", op_flock},
        {"op_getxattr", op_getxattr},     {"op_listxattr", op_listxattr}};

const std::string FuseTrace::operation_to_string(const operation_t operation) {
  return FuseTrace::operation_string_mapping.at(operation);
}

const operation_t FuseTrace::string_to_operation(const std::string operation) {
  return FuseTrace::string_operation_mapping.at(operation);
}