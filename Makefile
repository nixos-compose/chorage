CC=gcc
CXX=g++
DEBUG_FLAGS=-Og -ggdb3
# CFLAGS=$(DEBUG_FLAGS) -Wall -Werror `pkg-config --cflags --libs fuse3 glib-2.0`
# LDFLAGS=$(DEBUG_FLAGS) `pkg-config --cflags --libs fuse3 glib-2.0`
CFLAGS=-O3 -Wall -Werror `pkg-config --cflags --libs fuse3 glib-2.0`
LDFLAGS= `pkg-config --cflags --libs fuse3 glib-2.0`

# Default rule
all: chorage trace_player

tests: test_folder negative_cache_test multicast_test random_delay_test chrono_test sqlite_test

# Binary
chorage: chorage.o negative_cache.o multicast.o trace.o cache_log.o multicast_log.o cache_trace.o
	$(CXX) $(CFLAGS) -o $@ $^

trace_player: trace.o trace_player.o
	$(CXX) $(CFLAGS) -o $@ $^

negative_cache_test: negative_cache.o negative_cache_test.o
	$(CXX) $(CFLAGS) -o tests/$@ $^

multicast_test: multicast.o multicast_test.o multicast_log.o cache_trace.o
	$(CXX) $(CFLAGS) -o tests/$@ $^

random_delay_test: random_delay_test.o
	$(CXX) $(CFLAGS) -o tests/$@ $^

chrono_test: chrono_test.o
	$(CXX) $(CFLAGS) -o tests/$@ $^

sqlite_test: sqlite_test.o
	$(CXX) $(CFLAGS) -o tests/$@ $^ -lsqlite3

# Object files
dynamic_cache.o: dynamic_cache.c dynamic_cache.h
	$(CXX) $(CFLAGS) -c -D DEBUG_CACHE_SYSTEM_MUL=0 -D DEBUG_CACHE_SYSTEM=0 $< -o $@

dynamic_cache_no_cache.o: dynamic_cache.c dynamic_cache.h
	$(CXX) $(CFLAGS) -c -D NO_CACHE_SYS=1 $< -o $@

dynamic_cache_no_multicast.o: dynamic_cache.c dynamic_cache.h
	$(CXX) $(CFLAGS) -c -D NO_MULTICAST_SYS=1 $< -o $@

negative_cache.o: negative_cache.c negative_cache.h
	$(CXX) $(CFLAGS) -c -D DEBUG_CACHE_SYSTEM=0 $< -o $@

negative_cache_test.o: negative_cache_test.cc negative_cache.h
	$(CXX) $(CFLAGS) -c $< -o $@

multicast.o: multicast.c multicast.h cache_trace.hpp
	$(CXX) $(CFLAGS) -c -D DEBUG_CACHE_SYSTEM_MUL=0 $< -o $@

multicast_test.o: multicast_test.cc multicast.h
	$(CXX) $(CFLAGS) -c $< -o $@

random_delay_test.o: random_delay_test.cc
	$(CXX) $(CFLAGS) -c $< -o $@

chrono_test.o: chrono_test.cc
	$(CXX) $(CFLAGS) -c $< -o $@

sqlite_test.o: sqlite_test.cc
	$(CXX) $(CFLAGS) -c $< -o $@

trace.o : trace.cc trace.hpp
	$(CXX) $(CFLAGS) -c $< -o $@

trace_player.o: trace_player.cc trace_player.hpp chorage_inode.hpp settings.hpp
	$(CXX) $(CFLAGS) -c $< -o $@

cache_trace.o: cache_trace.cc cache_trace.hpp
	$(CXX) $(CFLAGS) -c $< -o $@

cache_log.o: cache_log.cc cache_log.hpp
	$(CXX) $(CFLAGS) -c $< -o $@

multicast_log.o: multicast_log.cc multicast_log.hpp
	$(CXX) $(CFLAGS) -c $< -o $@

chorage.o: chorage.cc cxxopts.hpp negative_cache.h multicast.h trace.hpp cache_log.hpp multicast_log.hpp cache_trace.hpp
	$(CXX) $(CFLAGS) -c $<

# Utility
clean:
	rm -f *.o chorage trace_player
	rm -rf tests

test_folder:
	mkdir -p tests