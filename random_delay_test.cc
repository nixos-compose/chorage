#include <chrono>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <map>
#include <random>
#include <string>

int main() {
  // Seed with a real random value, if available
  std::random_device r;

  // Choose a random mean between 1 and 6
  std::default_random_engine e1(r());
  std::uniform_int_distribution<int> uniform_dist(1, 200);

  std::map<int, int> hist;
  for (int n = 0; n != 10000; ++n) {
    auto start = std::chrono::high_resolution_clock::now();
    int value = uniform_dist(e1);
    (void) value;
    auto end = std::chrono::high_resolution_clock::now();
    ++hist[std::round(std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count())];
  }

  std::cout << "Distribution of computation time for the random number:\n"
            << std::fixed << std::setprecision(1);
  for (auto [x, y] : hist)
    std::cout << std::setw(2) << x << ' ' << std::string(y / 200, '*') << '\n';
}