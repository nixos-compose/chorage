#include "cache_trace.hpp"
#include "chorage_inode.hpp"
#include "settings.hpp"
#include <algorithm>
#include <bits/chrono.h>
#include <chrono>
#include <cstring>
#include <exception>
#include <fstream>
#include <iterator>
#include <map>
#include <stdexcept>
#include <sstream>
#include <string>
#include <vector>
using std::strerror;
#include <cerrno>

// The one shared instance of this class
CacheTrace cache_trace;

CacheTrace::CacheTrace() {
  trace.reserve(1000000);
}

void CacheTrace::append(const cache_operation_t operation, const ino_t parent,
                        const char *name) {
#ifdef CACHE_TRACE_ENABLE
  trace.push_back({.operation = operation,
                   .time = std::chrono::system_clock::now(),
                   .parent = parent,
                   .name = std::string(name)});
#else
  (void)operation;
  (void)parent;
  (void)name;
#endif
}

std::string trace_to_string(CacheTrace::trace_entry entry) {
  const auto now = std::chrono::duration_cast<std::chrono::microseconds>(
                       entry.time.time_since_epoch())
                       .count();
  std::stringstream sstring;
  sstring << CacheTrace::operation_to_string(entry.operation) << "," << now
          << "," << entry.parent << "," << entry.name;
  return sstring.str();
}

void CacheTrace::write_trace_to_file(std::string file_path) {
  std::ofstream output_file(file_path);
  if (!output_file.is_open())
    throw std::runtime_error(
        std::string("Failed to open the cache-trace file: ") + strerror(errno));

  std::ostream_iterator<std::string> output_iterator(output_file, "\n");
  std::transform(std::begin(trace), std::end(trace), output_iterator,
                 trace_to_string);
}

const std::unordered_map<cache_operation_t, std::string>
    CacheTrace::operation_string_mapping = {{miss, "miss"},
                                            {hit, "hit"},
                                            {sent_entry, "sent_entry"},
                                            {received_entry, "received_entry"}};

const std::unordered_map<std::string, cache_operation_t>
    CacheTrace::string_operation_mapping = {{"miss", miss},
                                            {"hit", hit},
                                            {"sent_entry", sent_entry},
                                            {"received_entry", received_entry}};

const std::string
CacheTrace::operation_to_string(const cache_operation_t operation) {
  return CacheTrace::operation_string_mapping.at(operation);
}

const cache_operation_t
CacheTrace::string_to_operation(const std::string operation) {
  return CacheTrace::string_operation_mapping.at(operation);
}