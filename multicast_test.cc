#include "multicast.h"
#include <iostream>
#include <unistd.h>

void receive_message(ino_t parent, std::string name, int error_number) {
  std::cout << "Received message (" << parent << ", " << name << ", "
            << error_number << ")" << std::endl;
}

int main(int argc, char *argv[]) {
#ifdef MULTICAST_LOG_ENABLE
  std::cout << "send and receive 10 times, every 5 seconds." << std::endl;
  init_multicast_sockets(receive_message);
  for (int i = 0; i < 10; ++i) {
    std::cout << "Sending message (" << (200 + 20 * i) << ", libcrypt.so, 3)"
              << std::endl;
    send_message(200 + 20 * i, "libcrypt.so", 3);
    sleep(2);
  }

  std::cout << "Multicast log (CSV):" << std::endl;
  multicast_log.print_csv();

  close_sockets();
#endif
}