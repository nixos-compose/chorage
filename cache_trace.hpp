#ifndef CACHE_TRACE_HPP
#define CACHE_TRACE_HPP

#include "chorage_inode.hpp"
#include <chrono>
#include <fstream>
#include <string>
#include <sys/stat.h> // for ino_t
#include <unordered_map>
#include <vector>

enum cache_operation_t { miss, hit, sent_entry, received_entry };

/**
 * A timed log of operations on the cache: hit, miss, sending and receiving
 * cache entries through multicast.
 *
 * It differs from CacheLog in that it notes when an event occurs, and the
 * information logged is based on Inodes, not full paths. This choice is imposed
 * by our interest in multicast operations, which cannot always map to full
 * paths.
*/
class CacheTrace {
public:
  CacheTrace();

  void append(const cache_operation_t operation, const ino_t parent,
              const char *name);
  void write_trace_to_file(std::string file_path);

  static const std::string
  operation_to_string(const cache_operation_t operation);
  static const cache_operation_t
  string_to_operation(const std::string operation);

  struct trace_entry {
    cache_operation_t operation;
    std::chrono::system_clock::time_point time;
    ino_t parent;
    std::string name;
  };

private:
  std::vector<trace_entry> trace;

  static const std::unordered_map<cache_operation_t, std::string>
      operation_string_mapping;
  static const std::unordered_map<std::string, cache_operation_t>
      string_operation_mapping;
};

extern CacheTrace cache_trace;

#endif