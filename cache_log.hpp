#ifndef CACHE_LOG_H
#define CACHE_LOG_H

#include <fstream>
#include <string>
#include <vector>

enum class CacheLogStatus { hit, miss };

/**
 * A log of all cache hits and misses, to be written to a file for performance
 * analysis
 */
class CacheLog {
public:
  CacheLog();

  struct LogEntry {
    CacheLogStatus status;
    std::string path;
    std::string name;
  };

  void append(LogEntry new_entry);
  void set_file_path(std::string file_path);
  bool write_file();
  void print_csv();

private:
  std::vector<LogEntry> log;
  std::ofstream output_file;
};

#endif // CACHE_LOG_H