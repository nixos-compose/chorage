// options pour le type de cache

#include <sys/stat.h>
#include <unistd.h>
#include <sys/types.h>

enum { WRONG_PATH = 0, GOOD_PATH = -1 };

void print_info(const char *format, ...);

void init_caches(void *lo);
void destroy_caches();

int in_open_cache(const ino_t parent, const char *name);
int in_stat_cache(long real_ino);

// 2 fonctions ?
void insert_caches(const ino_t parent, const char *name, long ino,
                   long real_ino, struct stat *st);
void update_cache_open(const ino_t parent, const char *name, long ino);
void update_cache_stat(long real_ino, struct stat *st);

// TODO invalidate -> set a true
void invalide_cache_open(const char *path);
void invalide_cache_open_2(const ino_t parent, const char *name);

unsigned long get_cache_open(const ino_t parent, const char *name);
void get_stat_cache_entry(long real_ino, struct stat *st);

void print_cache();
void check_error_lt(int checkInt, char *function, char *error_message);
