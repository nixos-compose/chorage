import socket
import struct

multicast_group = '224.0.0.1'
multicast_port = 1234
IS_ALL_GROUPS = True

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
try:
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
except AttributeError:
    print("no reuse port")
    pass # Some systems don't support SO_REUSEPORT
sock.setsockopt(socket.SOL_IP, socket.IP_MULTICAST_LOOP, 1)

if IS_ALL_GROUPS:
    # on this port, receives ALL multicast groups
    sock.bind(('', multicast_port))
else:
    # on this port, listen ONLY to multicast_group
    sock.bind((multicast_group, multicast_port))

mreq = struct.pack("4sL", socket.inet_aton(multicast_group), socket.INADDR_ANY)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

while True:
    # For Python 3, change next line to "print(sock.recv(10240))"
    print("waiting to receive message")
    data, address = sock.recvfrom(1024)

    print('received {} bytes from {}'.format(
        len(data), address))
    print(data)

    print('sending acknowledgement to', address)
    sock.sendto(b'ack', address)