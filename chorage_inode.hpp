#ifndef CHORAGE_INODE_HPP
#define CHORAGE_INODE_HPP

#include "settings.hpp"
#include <cstdint>
#include <mutex>
#include <sys/stat.h>
#include <unistd.h>

struct Inode {
  int fd{-1};
  dev_t src_dev{0};
  ino_t src_ino{0};
  int generation{0};
  uint64_t nopen{0};
  uint64_t nlookup{0};
  std::mutex m;
#if defined TRACING_ENABLE || defined CACHE_LOG_ENABLE
  std::string path;
#endif

  // Delete copy constructor and assignments. We could implement
  // move if we need it.
  Inode() = default;
  Inode(const Inode &) = delete;
  Inode(Inode &&inode) = delete;
  Inode &operator=(Inode &&inode) = delete;
  Inode &operator=(const Inode &) = delete;

  ~Inode() {
    if (fd > 0)
      close(fd);
  }
};
#endif