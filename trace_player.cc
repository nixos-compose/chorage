// C headers
#include <dirent.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

// C++ headers
#include "cxxopts.hpp"
#include "trace.hpp"
#include "trace_player.hpp"
#include <algorithm>
#include <cstdio>
#include <exception>
#include <fstream>
#include <iostream>
#include <iterator>
#include <stdexcept>
#include <string>

TracePlayer::TracePlayer(std::string trace_path) : trace_path(trace_path) {
  std::ifstream trace_file(trace_path);

  std::string line;
  while (getline(trace_file, line)) {
    std::pair<operation_t, std::string> parsed_line = parse_trace_line(line);
    do_operation(parsed_line.first, parsed_line.second);
  }
  // std::for_each(std::istream_iterator<std::string>(trace_file),
  //               std::istream_iterator<std::string>(), [this](std::string
  //               line) {
  //                 std::pair<operation_t, std::string> parsed_line =
  //                     parse_trace_line(line);
  //                 do_operation(parsed_line.first, parsed_line.second);
  //               });
}

std::pair<operation_t, std::string>
TracePlayer::parse_trace_line(std::string line) {
  std::string delimiter = ":";
  size_t delimiter_location = 0;

  if ((delimiter_location = line.find(delimiter)) == std::string::npos) {
    throw std::runtime_error(
        "Not nice, the string passed to parse_trace_line does "
        "not have the expected delimiter.");
  }

  return std::make_pair(FuseTrace::string_to_operation(line.substr(0, delimiter_location)),
                        line.substr(delimiter_location + delimiter.length()));
}

void TracePlayer::do_operation(operation_t operation, std::string path) {
  switch (operation) {
  case op_getattr:
    struct stat attr;
    fstatat(AT_FDCWD, path.c_str(), &attr, AT_SYMLINK_NOFOLLOW);
    break;
  case op_lookup:
    open(path.c_str(), O_PATH | O_NOFOLLOW);
    break;
  case op_readlink:
    char buf[PATH_MAX + 1];
    long int readlink_res;
    readlink_res = readlink(path.c_str(), buf, sizeof(buf));
    (void)readlink_res;
    break;
  case op_opendir:
    open_directories[path] = opendir(path.c_str());
    break;
  case op_readdir:
    std::cerr << "readdir not implemented. It is too complex for now to do."
              << std::endl;
    break;
  case op_readdirplus:
    std::cerr << "readdirplus not implemented. It is too complex for now to do."
              << std::endl;
    break;
  case op_releasedir:
    if (auto dir = open_directories.find(path); dir != open_directories.end()) {
      free(dir->second);
      open_directories.erase(dir);
    } else {
      std::cerr << "Trying to release directory " << path
                << " which is not in the map of open directories" << std::endl;
    }
    break;
  case op_open:
    int fd;
    fd = open(path.c_str(), 0);
    if (fd != -1)
      open_files[path] = fd;
    else
      std::cerr << "could not open the file " << path << std::endl;
    break;
  case op_release:
    std::cerr << "not processed" << std::endl;
    break;
  case op_read:
    std::cerr << "not processed" << std::endl;
    break;
  case op_statfs:
    std::cerr << "not processed" << std::endl;
    break;
  case op_flock:
    std::cerr << "not processed" << std::endl;
    break;
  case op_getxattr:
    std::cerr << "not processed" << std::endl;
    break;
  case op_listxattr:
    std::cerr << "not processed" << std::endl;
    break;
  default:
    std::cerr << "Operation " << FuseTrace::operation_to_string(operation)
              << " is not supported yet for replay." << std::endl;
  }
}

static void print_usage(char *prog_name) {
  std::cout << "Usage: " << prog_name << " --help\n"
            << "       " << prog_name << " [options] <trace_file>\n";
}

static cxxopts::ParseResult parse_wrapper(cxxopts::Options &parser, int &argc,
                                          char **&argv) {
  try {
    return parser.parse(argc, argv);
  } catch (cxxopts::option_not_exists_exception &exc) {
    std::cout << argv[0] << ": " << exc.what() << std::endl;
    print_usage(argv[0]);
    exit(2);
  }
}

static cxxopts::ParseResult parse_options(int argc, char **argv) {
  cxxopts::Options opt_parser(argv[0]);
  // clang-format off
  opt_parser.add_options()
    ("help", "Print help");
  // clang-format on

  // FIXME: Find a better way to limit the try clause to just
  // opt_parser.parse() (cf. https://github.com/jarro2783/cxxopts/issues/146)
  auto options = parse_wrapper(opt_parser, argc, argv);

  if (options.count("help")) {
    print_usage(argv[0]);
    // Strip everything before the option list from the
    // default help string.
    auto help = opt_parser.help();
    std::cout << std::endl
              << "options:"
              << help.substr(help.find("\n\n") + 1, std::string::npos);
    exit(0);
  } else if (argc != 2) {
    std::cout << argv[0] << ": invalid number of arguments\n";
    print_usage(argv[0]);
    exit(2);
  }
  return options;
}

int main(int argc, char *argv[]) {
  // Parse command line options
  auto options{parse_options(argc, argv)};

  TracePlayer player(argv[1]);
  return 0;
}