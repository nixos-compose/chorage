#ifndef SETTINGS_HPP
#define SETTINGS_HPP

// Enable or not
// #define TRACING_ENABLE // Record the system calls processed by Chorage
// #define CACHE_LOG_ENABLE // Log all cache hit and miss
// #define MULTICAST_LOG_ENABLE // count the number of multicast messages sent and received
// #define RANDOM_DELAY_LOOKUP_ENABLE // Add a random delay to lookup operations after cache miss and before system calls
#define CACHE_TRACE_ENABLE // Record when a given inode is asked for, found to be missing, the related message is sent and then received

#endif