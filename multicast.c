#include "multicast.h"
#include <arpa/inet.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <pthread.h>
#include <stdarg.h>
#include <string>
#include <sys/socket.h>
#include <unistd.h>

#ifdef CACHE_TRACE_ENABLE
#include "cache_trace.hpp"
#endif

#ifdef MULTICAST_LOG_ENABLE
MulticastLog multicast_log;
#endif

#ifndef IP_MULTICAST_GROUP
#define IP_MULTICAST_GROUP "233.0.1.2"
#endif
#ifndef PORT_MULTICAST_GROUP
#define PORT_MULTICAST_GROUP 19437
#endif

#ifndef DEBUG_CACHE_SYSTEM_MUL
#define DEBUG_CACHE_SYSTEM_MUL 0
#endif

// max path size is use for send messages
#define MAX_NAME_LENGTH 255
// Size of the string used to identify the sender of a message
#ifdef MULTICAST_LOG_ENABLE
#define SENDER_NAME_LENGTH 25
#endif

/* ERROR MANAGEMENT */
void check_error_lt(int checkInt, const char *function,
                    const char *error_message) {
  if (checkInt < 0) {
    fprintf(stderr, "Dynamic_cache.c : %s : %s\n", function, error_message);
    exit(checkInt);
  }
}
void check_error_eq(void *checkInt, const char *function,
                    const char *error_message) {
  if (checkInt == 0) {
    fprintf(stderr, "Dynamic_cache.c : %s : %s\n", function, error_message);
    exit(1);
  }
}
void check_error_neq(int checkInt, const char *function,
                     const char *error_message) {
  if (checkInt != 0) {
    fprintf(stderr, "Dynamic_cache.c : %s : %s\n", function, error_message);
    exit(1);
  }
}

/* DEBUG MANAGEMENT */
void print_info_mul(const char *format, ...) {
#if DEBUG_CACHE_SYSTEM_MUL == 1
  va_list args;
  va_start(args, format);
  vprintf(format, args);
  va_end(args);
#endif
}

static pthread_t waiter;

// send socket
int send_sockfd, recv_sockfd;
struct sockaddr_in send_addr, recv_addr;

typedef struct {
  ino_t parent;
  int name_length;
  char name[MAX_NAME_LENGTH];
  int error_number;
#ifdef MULTICAST_LOG_ENABLE
  char message_sender[SENDER_NAME_LENGTH];
#endif
} message_t;

// Forward declaration for init_multicast_system
void init_send_socket();
void init_recv_socket();
void *fn_waiter(void *waiter_data);
/**
 * Initialise the multicast system by creating and configuring sockets for
 * sending and receiving multicast messages, joining the multicast group, and
 * creating a thread for waiting for incoming multicast messages.
 *
 * @param insert_negative_cache data given to the thread at initialisation. If
 *        not null, it is a pointer to a function that receives the message
 *        data as parameter.
 */
void init_multicast_sockets(void (*insert_negative_cache)(const ino_t parent,
                                                          std::string name,
                                                          int error_nb)) {
  init_send_socket();
  init_recv_socket();
  print_info_mul(
      "init_multicast_system : initialisation complete, multicast group "
      "has been joined\n");

  int r =
      pthread_create(&waiter, NULL, fn_waiter, (void *)insert_negative_cache);
  check_error_neq(r, "init_multicast_system",
                  "Impossible de créer le thread multicast !");
}

void init_send_socket() {
  // Create send socket
  send_sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  check_error_lt(send_sockfd, "init_multicast_system",
                 "Failed to create send socket.");

  // Configure address for sending
  memset(&send_addr, 0, sizeof(send_addr));
  send_addr.sin_family = AF_INET;
  send_addr.sin_addr.s_addr =
      inet_addr(IP_MULTICAST_GROUP); // associe pour toutes les interfaces
  send_addr.sin_port = htons(PORT_MULTICAST_GROUP);

  // int send_reuse = 1;
  // int res = setsockopt(send_sockfd, SOL_SOCKET, SO_REUSEADDR,
  //                      (char *)&send_reuse, sizeof(send_reuse));
  // check_error_lt(res, "init_multicast_system",
  //                "Failed to configure send socket to reuse address.");

  // Prevent messages we send from looping back
  int loop = 0;
  int res = setsockopt(send_sockfd, IPPROTO_IP, IP_MULTICAST_LOOP, &loop,
                       sizeof(loop));
  check_error_lt(res, "init_multicast_system", "Erreur reuse socket recv");

  print_info_mul("init_multicast_system : socket émission initialisée\n");
}

void init_recv_socket() {
  recv_sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  if (recv_sockfd < 0)
    perror("socket");

  unsigned int recv_reuse = 1;
  if (setsockopt(recv_sockfd, SOL_SOCKET, SO_REUSEADDR, (char *)&recv_reuse,
                 sizeof(recv_reuse)) < 0)
    perror("init_multicast_system: allow reuse of port number");

  // Set the address for the socket
  memset(&recv_addr, 0, sizeof(recv_addr));
  recv_addr.sin_family = AF_INET;
  recv_addr.sin_addr.s_addr =
      htonl(INADDR_ANY); // receive messages from all network interfaces
  recv_addr.sin_port = htons(PORT_MULTICAST_GROUP);

  // Bind socket to the multicast address
  if (bind(recv_sockfd, (struct sockaddr *)&recv_addr, sizeof(recv_addr)) < 0)
    perror("init_multicast_system: bind to multicast address and port");

  // Request to join a multicast group
  struct ip_mreqn mreq;
  mreq.imr_multiaddr.s_addr = inet_addr(IP_MULTICAST_GROUP);
  mreq.imr_address.s_addr = htonl(INADDR_ANY);
  mreq.imr_ifindex = 0;
  if (setsockopt(recv_sockfd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq,
                 sizeof(mreq)) < 0)
    perror("init_multicast_system: register to multicast group");
}

// Forward declaration for fn_waiter
void receive_message(void (*insert_negative_cache)(const ino_t parent,
                                                   std::string name,
                                                   int error_nb));
/**
 * Function of a thread that continuously receives messages and closes the
 * socket when finished.
 *
 * @param waiter_data data given to the thread at initialisation. If not null,
 *        it is a pointer to a function that receives the message data as
 *        parameter.
 */
void *fn_waiter(void *waiter_data) {
  print_info_mul("fn_waiter : Waiter thread initialisation\n");
  while (1) {
    receive_message(
        (void (*)(const ino_t parent, std::string name,
                  int error_nb))waiter_data); // recv_sockfd is global
  }
  print_info_mul("fn_waiter : waiter thread end, sockets has been closed\n");
}

/**
 * Receive messages over a socket and handle different types of messages.
 */
void receive_message(void (*insert_negative_cache)(const ino_t parent,
                                                   std::string name,
                                                   int error_nb)) {
  message_t m;

  unsigned int addr_len = sizeof(recv_addr);
  long unsigned int message_len = recvfrom(
      recv_sockfd, &m, sizeof(m), 0, (struct sockaddr *)&recv_addr, &addr_len);
  if (message_len < 0) {
    perror("receive_message: recvfrom");
    return;
  }

  print_info_mul("New message incoming:\n");

  if (message_len < sizeof(m))
    printf("receive_message: The received message's size does not match the "
           "expected size\n");

  ino_t parent = m.parent;
  print_info_mul("\tparent inode: %d\n", parent);

  // Extract length of the entry's name
  // Note : MAX_NAME_LENGTH includes the \0 char
  int name_length = m.name_length;
  check_error_neq(name_length > MAX_NAME_LENGTH, "receive_message",
                  "Lookup name (field of received message) is larger than max "
                  "allowed size.");
  // Extract entry's name
  char *name = (char *)calloc(1, name_length);
  check_error_eq(name, "receive_message", "calloc failed");
  memcpy(name, m.name, name_length);
  print_info_mul("\tlookup entry name : %s\n", name);

  char error_number = m.error_number;
  print_info_mul("\terror number : %d\n", error_number);
#ifdef MULTICAST_LOG_ENABLE
  print_info_mul("\tmessage_sender: %s\n", m.message_sender);
#endif
  print_info_mul("Message ends\n");

#ifdef MULTICAST_LOG_ENABLE
  multicast_log.increment(m.message_sender);
#endif

  if (insert_negative_cache != NULL)
    (*insert_negative_cache)(parent, name, error_number);
#ifdef CACHE_TRACE_ENABLE
  cache_trace.append(received_entry, parent, name);
#endif
}

/**
 * Send information about a path (parent inode + name) to
 * the multicast group for use during lookup.
 *
 * WARNING : the length of name is limited to MAX_NAME_LENGTH
 *
 * @param parent inode of the folder in which we look for name
 * @param name the file/folder/whatever we are looking for in the parent
 * @param error_number the errno of the failed attempt to open name in parent
 */
void send_message(const ino_t parent, const char *name, int error_number) {
  int name_length = strlen(name) + 1; //+1 for the \0

  if (name_length > MAX_NAME_LENGTH) {
    print_info_mul("share : name_length too long, message not sent");
    return;
  }

  print_info_mul("New message sent:\n");
  message_t m;
  memset(&m, 0, sizeof(message_t));

  m.parent = parent;
  print_info_mul("\tparent inode : %u\n", m.parent);

  m.name_length = name_length;
  memcpy(&(m.name), name, name_length);
  print_info_mul("\tlookup entry name : %s (%d chars)\n", m.name,
                 m.name_length);

  m.error_number = error_number;
  print_info_mul("\terror number : %d\n", m.error_number);

#ifdef MULTICAST_LOG_ENABLE
  gethostname(m.message_sender, SENDER_NAME_LENGTH - 1);
  // Make sure that the string is terminated, as gethostname does not enforce it
  m.message_sender[SENDER_NAME_LENGTH - 1] = 0;
  print_info_mul("\tmessage_sender: %s\n", m.message_sender);
#endif

  print_info_mul("Writing message: ");
  int r = sendto(send_sockfd, &m, sizeof(message_t), 0,
                 (struct sockaddr *)&send_addr, sizeof(send_addr));
  print_info_mul("sent %d bytes of a message of size %d.\n", r,
                 sizeof(message_t));
  check_error_lt(r, "share", "Failed to send cache information.");

  print_info_mul("End of sent message.\n");

#ifdef MULTICAST_LOG_ENABLE
  multicast_log.increment("self");
#endif
}

/**
 * Cleanly close the two sockets
 */
void close_sockets() {
  pthread_cancel(waiter);
  close(send_sockfd);
  close(recv_sockfd);
}