{stdenv, fuse3, glib, pkg-config, sqlite}:
stdenv.mkDerivation {
  name = "chorage";
  src = ./.;

  buildInputs = [
    fuse3
    glib
    sqlite
  ];

  nativeBuildInputs = [
    pkg-config
  ];

  # This is the default, so we don't need it.
  buildPhase =
  ''
    runHook preBuild
    make
    make tests
    runHook postBuild
  '';

  # The default install phase is make install (TODO)
  installPhase = ''
    runHook preInstall
    mkdir -p $out/bin
    mv tests/multicast_test $out/bin
    mv chorage $out/bin
    runHook postInstall
  '';
}