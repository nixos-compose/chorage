# Chorage is a FUSE-based metadata cache that can be distributed

The source `chorage.cc` is based on `passthrough_hp.cc` from FUSE's example code. It mostly relays (hence the word passthrough) the incoming file system requests to another file system already mounted. We altered it to trace the metadata operations, to cache the metadata (including whether a file or path is non-existant) and to distribute that cache instances running on several machines (through a multicast protocol).

Chorage is meant to be mounted on a read-only file system. It does not support modification of the data nor metadata of the file system it reads from.

## Trying chorage on an example

The different instances of Chorage need to run on different machines unless `int loop = 0;` is set to 1 in `multicast.c`.

1. start Chorage
  1. `./chorage experimentations/test_store experimentations/test_mount1 --cache-trace /absolute/path/to/kt1`
  2. `./chorage experimentations/test_store experimentations/test_mount2 --cache-trace /absolute/path/to/kt2`
2. run the test script, which tries to reach missing paths `bash experimentations/chorage_test.sh`
3. unmount the chorage mount points
  1. `umount experimentations/test_store`
  2. `umount experimentations/test_store2`
4. inspect the cache trace files `kt1` and `kt2`