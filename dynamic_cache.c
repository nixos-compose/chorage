#include "dynamic_cache.h"

#include <arpa/inet.h>
#include <errno.h>
#include <glib.h>
#include <pthread.h>
#include <stdarg.h>

#ifdef NO_CACHE_SYS
#define NO_CACHE 1
#else
#define NO_CACHE 0
#endif

#ifdef NO_MULTICAST_SYS
#define NO_MULTICAST 1
#else
#define NO_MULTICAST 0
#endif

#ifndef IP_MULTICAST_GROUP
#define IP_MULTICAST_GROUP "233.0.1.2"
#endif
#ifndef PORT_MULTICAST_GROUP
#define PORT_MULTICAST_GROUP 19437
#endif

#ifndef DEBUG_CACHE_SYSTEM
#define DEBUG_CACHE_SYSTEM 0
#endif

#ifndef DEBUG_CACHE_SYSTEM_MUL
#define DEBUG_CACHE_SYSTEM_MUL 0
#endif

// max path size is use for send messages
#define MAX_PATH_SIZE 1000

/* ERROR MANAGMENT */
void check_error_lt(int checkInt, const char *function,
                    const char *error_message) {
  if (checkInt < 0) {
    fprintf(stderr, "Dynamic_cache.c : %s : %s\n", function, error_message);
    exit(checkInt);
  }
}
void check_error_eq(void *checkInt, const char *function,
                    const char *error_message) {
  if (checkInt == 0) {
    fprintf(stderr, "Dynamic_cache.c : %s : %s\n", function, error_message);
    exit(1);
  }
}
void check_error_neq(int checkInt, const char *function,
                     const char *error_message) {
  if (checkInt != 0) {
    fprintf(stderr, "Dynamic_cache.c : %s : %s\n", function, error_message);
    exit(1);
  }
}

/* DEBUG MANAGMENT */
void print_info(const char *format, ...) {
  if (!DEBUG_CACHE_SYSTEM)
    return;
  va_list args;
  va_start(args, format);
  vprintf(format, args);
  va_end(args);
}
void print_info_mul(const char *format, ...) {
  if (!DEBUG_CACHE_SYSTEM_MUL)
    return;
  va_list args;
  va_start(args, format);
  vprintf(format, args);
  va_end(args);
}

// definition de la table de hash
static GHashTable *cache_open;
static GHashTable *cache_stat;

// gestion des threads :
static pthread_mutex_t mutex_open;
static pthread_mutex_t mutex_stat;
static pthread_t waiter;

// send socket
int send_sockfd, sockfd;
struct sockaddr_in send_addr, addr;

typedef struct message_t {
  ino_t parent;
  int name_length;
  char name[MAX_PATH_SIZE];
  int value; // whether a the entry `name` exists as a child of inode `parent`.
  struct stat stat;
} message;

// key & value destroying
void free_key_value(gpointer data) { free(data); }

/**
 * The function `receive_message` receives messages over a socket and handles
 * different types of messages based on their type.
 *
 * @param lo The parameter "lo" is a void pointer, which means it can point to
 * any type of data. It is used as an argument for the "call_back_inode"
 * function, which suggests that it might be used to pass some context or data
 * to that function. Without more information about the code,
 * @param sockfd The parameter `sockfd` is the file descriptor of the socket
 * from which the message is received.
 */
void receive_message(int sockfd) {
  message m;

  int message_len = recv(sockfd, &m, sizeof(message), 0);
  check_error_lt(message_len, "receive_message", "Erreur lecture du message");

  print_info_mul("receive_message : new message incoming ...");

  // Extract length of directory entry's name
  // PATHLEN includes the \0 char
  int name_length = m.name_length;
  check_error_neq(name_length > MAX_PATH_SIZE, "receive_message",
                  "Taille de message dépassant la taille max");
  print_info_mul("\t directory entry name : %d\n", name_length);

  // Extract directory entry's name
  char *name = (char *)calloc(1, name_length);
  check_error_eq(name, "receive_message",
                 "dynamic_cache : receive_message : calloc fail");
  memcpy(name, m.name, name_length);
  print_info_mul("\t name : %s\n", name);

  char value = m.value;
  print_info_mul("\t value : %d\n", value);

  pthread_mutex_lock(&mutex_open);
  if (!g_hash_table_contains(cache_open, name)) {
    long *val = (long *)calloc(1, sizeof(long));
    check_error_eq(name, "receive_message",
                   "dynamic_cache : receive_message : calloc fail");

    if (value) {
      print_info_mul("\t New value not null in cache ...\n");
      *val = (long)GOOD_PATH;
    } else {
      print_info_mul("\t New value null path in cache ...\n");
      *val = (long)WRONG_PATH;
    }
    g_hash_table_insert(cache_open, name, val);
    print_info_mul("\t Insert in table : %s -> %ld ...\n", name, *val);
  } else {
    print_info_mul("\t Already in cache : %s\n", name);
    // TODO log the umber of occurrences of this situation.
    free(name);
  }
  pthread_mutex_unlock(&mutex_open);

  if (value) { // the file at name exist
    struct stat *st = (struct stat *)calloc(1, sizeof(struct stat));
    check_error_eq(name, "receive_message",
                   "dynamic_cache : receive_message : calloc fail");
    memcpy(st, &(m.stat), sizeof(struct stat));
    print_info_mul("\t Struct d'inode : %ld\n", st->st_ino);

    pthread_mutex_lock(&mutex_stat);
    if (!g_hash_table_contains(cache_stat, &(st->st_ino))) {
      long *ino = (long *)calloc(1, sizeof(long));
      check_error_eq(name, "receive_message",
                     "dynamic_cache : receive_message : calloc fail");
      *ino = st->st_ino;

      print_info_mul("\t Has been add to the cache !\n");
      g_hash_table_insert(cache_stat, ino, st);
    } else {
      print_info_mul("\t Already in cache !\n");
      free(st);
    }
    pthread_mutex_unlock(&mutex_stat);
  }
  print_info_mul("Message ends\n");
}

/**
 * The function `fn_waiter` is a thread that continuously receives messages and
 * closes the socket when finished.
 *
 * @param lo The parameter "lo" is a void pointer, which means it can be used to
 * pass any type of data to the function. In this case, it is being used to pass
 * some data to the "fn_waiter" function. The specific type and purpose of the
 * data being passed would need to
 */
void *fn_waiter(void *) {
  print_info("fn_waiter : Waiter thread initialisation\n");
  while (1) {
    receive_message(sockfd); // sockfd is global
  }
  close(sockfd);

  print_info("fn_waiter : waiter thread end, sockets has been closed\n");
}

/**
 * The function `init_multicast_system` initializes the multicast system by
 * creating and configuring sockets for sending and receiving multicast
 * messages, joining the multicast group, and creating a thread for waiting for
 * incoming multicast messages.
 *
 * @param lo The parameter "lo" is a void pointer, which means it can point to
 * any type of data. In this case, it is passed as an argument to the function
 * "fn_waiter" when creating a new thread using pthread_create. The purpose of
 * this parameter is to pass additional data or context
 */
void init_multicast_system(void *lo) {
  if (NO_MULTICAST)
    return;

  // Création du socket pour l'envoi
  send_sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  check_error_lt(send_sockfd, "init_multicast_system",
                 "Erreur lors de la création du socket d'envoi");

  // Configuration de l'adresse d'envoi
  memset(&send_addr, 0, sizeof(send_addr));
  send_addr.sin_family = AF_INET;
  send_addr.sin_addr.s_addr =
      inet_addr(IP_MULTICAST_GROUP); // associe pour toutes les interfaces
  send_addr.sin_port = htons(PORT_MULTICAST_GROUP);

  int send_reuse = 1;
  int res = setsockopt(send_sockfd, SOL_SOCKET, SO_REUSEADDR,
                       (char *)&send_reuse, sizeof(send_reuse));
  check_error_lt(res, "init_multicast_system", "Erreur reuse socket send");

  print_info("init_multicast_system : socket émission initialisée\n");

  // RECEPTION
  sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  check_error_lt(sockfd, "init_multicast_system",
                 "Impossible de créer le socket multicast !");
  struct ip_mreq mreq;

  // Configuration de l'adresse de la socket
  memset(&addr, 0, sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr =
      htonl(INADDR_ANY); // associe pour toutes les interfaces
  addr.sin_port = htons(PORT_MULTICAST_GROUP);

  int recv_reuse = 1;
  res = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (char *)&recv_reuse,
                   sizeof(recv_reuse));
  check_error_lt(res, "init_multicast_system", "Erreur reuse socket recv");

  // empêche de recevoir les messages qu'on envoie
  int loop = 0;
  res = setsockopt(sockfd, IPPROTO_IP, IP_MULTICAST_LOOP, &loop, sizeof(loop));
  check_error_lt(res, "init_multicast_system", "Erreur reuse socket recv");
  res = setsockopt(send_sockfd, IPPROTO_IP, IP_MULTICAST_LOOP, &loop,
                   sizeof(loop));
  check_error_lt(res, "init_multicast_system", "Erreur reuse socket recv");

  res = bind(sockfd, (struct sockaddr *)&addr, sizeof(addr));
  check_error_lt(res, "init_multicast_system",
                 "Erreur lors de l'attachement de l'adresse à la socket");

  // Configuration de la structure ip_mreq pour rejoindre le groupe multicast
  mreq.imr_multiaddr.s_addr = inet_addr(IP_MULTICAST_GROUP);
  mreq.imr_interface.s_addr = htonl(INADDR_ANY);

  // Ajout de l'interface au groupe multicast
  res = setsockopt(sockfd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *)&mreq,
                   sizeof(mreq));
  check_error_lt(res, "init_multicast_system",
                 "Erreur lors de l'ajout de l'interface au groupe multicast");

  print_info("init_multicast_system : initialisation complete, multicast group "
             "has been joined\n");

  int r = pthread_create(&waiter, NULL, fn_waiter, lo);
  check_error_neq(r, "init_multicast_system",
                  "Impossible de créer le thread multicast !");
}

/**
 * The function initializes caches.
 *
 * @param lo The parameter "lo" is a pointer to the starting address of the
 * memory region where the caches need to be initialized.
 */
void init_caches(void *lo) {
  if (NO_CACHE)
    return;
  cache_open = g_hash_table_new_full(g_str_hash, g_str_equal, free_key_value,
                                     free_key_value); // sould be .._new_full ?
  cache_stat = g_hash_table_new_full(g_int64_hash, g_int64_equal,
                                     free_key_value, free_key_value);

  int r1 = pthread_mutex_init(&mutex_stat, NULL);
  int r2 = pthread_mutex_init(&mutex_open, NULL);
  check_error_neq(r1 || r2, "init_caches",
                  "Impossible d'initialiser les mutexs !");

  print_info("init_caches : mutexs caches initialisés\n");

  if (NO_MULTICAST)
    return;
  init_multicast_system(lo);
}

/**
 * The function sets the path by concatenating the parent path and the name.
 *
 * We are going to do a hack here. The bytes of parent will be put in the "path"
 * string as if they were characters, but they are not meant to be. It should
 * however not confuse the hash function of cache_open as we finish with a
 * proper string with its final zero byte. This spares us the creation of a
 * custom hash function and ensuring that it does not create too many collision.
 *
 * @param parent The parent inode.
 * @param name The name of the file or directory that you want to set the path
 * for.
 */
char *path_to_string(const ino_t parent, const char *name) {
  int len = sizeof(ino_t) + strlen(name) + 1;
  char *path = (char *)calloc(1, len);
  check_error_eq(path, "path_to_string", "calloc fail");
  memcpy(path, &parent, sizeof(ino_t));
  strcpy(path + sizeof(ino_t), name);
  return path;
}

/**
 * The function `share` sends information about a path (parent inode + name) to
 * the multicast group for use during lookup.
 *
 * WARNING : the length of name is limited to MAX_PATH_SIZE
 *
 * @param parent inode of the folder in which we look for name
 * @param name the file/folder/whatever we are looking for in the parent
 * @param st pointer to the stat information of the given path
 */
void share_cache_entry(const ino_t parent, const char *name,
                       struct stat *st) {
  int name_length = strlen(name) + 1; //+1 for the \0

  if (name_length > MAX_PATH_SIZE) {
    print_info("share : name_length too long, message not sent");
    return;
  }

  print_info_mul("New message shared...\n");

  message m;
  memset(&m, 0, sizeof(message));
  m.parent = parent;
  print_info_mul("\t parent inode : %u ...\n", m.parent);
  m.name_length = name_length;
  memcpy(&(m.name), name, name_length);
  print_info_mul("\t name : %s (%d chars) ...\n", m.name, m.name_length);
  m.value = (st != NULL);
  print_info_mul("\t value : %d ...\n", m.value);
  if (st != NULL) {
    memcpy(&(m.stat), st, sizeof(struct stat));
    print_info_mul("\t stat ino : %ld ...\n", (m.stat).st_ino);
  }

  print_info_mul("\t Writing message ...\n");
  int r = sendto(send_sockfd, &m, sizeof(message), 0,
                 (struct sockaddr *)&send_addr, sizeof(send_addr));
  check_error_lt(r, "share", "Failed to send cache information");

  print_info_mul("End message shared...\n");
}

static int openCount = 0;
static int statCount = 0;
/**
 * The function records a full path in the open cache and the file status
 * information in the stat cache.
 *
 * It involves successively taking the locks mutex_open and the mutex_stat.
 *
 * @param parent Inode of the parent directory of the `name` entry. It is the
 *        Inode in the underlying file system which we assume to be the same
 *        for all participant in the shared cache.
 * @param name The name of the cache to be inserted.
 * @param ino The internal FUSE inode number of the file or directory being
 *        inserted into the cache or GOOD_PATH or WRONG_PATH.
 * @param real_ino The inode number from the perspective of the underlying file
 *        system of the file or directory being inserted into the cache or
 *        WRONG_PATH.
 * @param st The "st" parameter is a pointer to a struct stat object. This
 *        object contains information about the file or directory being inserted
 *        into the cache. It typically includes attributes such as file size,
 *        permissions, timestamps, and more. Can be set to NULL to mean that there
 *        is no status information to cache.
 */
void insert_caches(const ino_t parent, const char *name, long ino,
                   long real_ino, struct stat *st) {
  if (NO_CACHE)
    return;

  // Make a cache key from the parent inode and the `name`
  char *path = path_to_string(parent, name);

  long *inode_cache_entry = (long *)calloc(1, sizeof(long));
  check_error_eq(inode_cache_entry, "insert_cache", "calloc fail");

  *inode_cache_entry = ino;

  // Save this inode in the cache
  pthread_mutex_lock(&mutex_open);
  g_hash_table_insert(cache_open, path, inode_cache_entry);
  openCount++;
  pthread_mutex_unlock(&mutex_open);

  print_info("insert_cache : add %d of path %s with fuse inode %ld\n",
             openCount, path, ino);

  // Cache status information, if relevant
  if (st != NULL) {
    update_cache_stat(real_ino, st);
  }

  // Send the cache information other the network
  share_cache_entry(parent, name, st);

  print_cache();
}

/**
 * The function checks if a cache is open with the given parent and name.
 *
 * @param parent inode of the folder in which we are looking for
 * @param name name of the directory entry we are looking for
 *
 * @return 0 if not in the cache
 *         1 if in the cache and the answer is positive but not yet open
 *         (val = -1) received by multicast
 *         2 if in the cache and negative answer (val = 0)
 *         3 if in the cache and positive answer, open or not (val = num inode)
 */
int in_open_cache(const ino_t parent, const char *name) {
  if (NO_CACHE)
    return 0;
  char *path = path_to_string(parent, name);

  print_info("in_open_cache : searching for entry %s \n", path);

  pthread_mutex_lock(&mutex_open);
  if (g_hash_table_contains(cache_open, path)) {
    long cache_value = *((long *)g_hash_table_lookup(cache_open, path));
    pthread_mutex_unlock(&mutex_open);
    free(path);

    switch (cache_value) {
    case GOOD_PATH:
      return 1;
    case WRONG_PATH:
      return 2;
    default:
      return 3;
    }
    return (cache_value == 0) ? 2 : 3;
  } else {
    pthread_mutex_unlock(&mutex_open);
    free(path);
    return 0;
  }
}

/**
 * The function checks if a cache stat contains a specific real inode number.
 *
 * @param real_ino The parameter "real_ino" is a long integer that represents
 * the real inode number.
 */
int in_stat_cache(long real_ino) {
  if (NO_CACHE)
    return 0;

  print_info("in_stat_cache : check de l'élément %ld \n", real_ino);
  pthread_mutex_lock(&mutex_stat);
  int tmp = g_hash_table_contains(cache_stat, &real_ino);
  pthread_mutex_unlock(&mutex_stat);
  return tmp;
}

/**
 * The function fills a struct stat with information about a file using a given
 * inode number.
 *
 * @param real_ino The real_ino parameter is a long integer that represents the
 * inode number of a file or directory.
 * @param st The parameter "st" is a pointer to a struct of type "stat". This
 * struct is used to store information about a file or file system object, such
 * as its size, permissions, and timestamps. By passing a pointer to this struct
 * as a parameter, the function can modify the struct and store
 */
void get_stat_cache_entry(long real_ino, struct stat *st) {
  if (NO_CACHE)
    return;

  pthread_mutex_lock(&mutex_stat);
  struct stat *tmp = (struct stat *)g_hash_table_lookup(cache_stat, &real_ino);
  pthread_mutex_unlock(&mutex_stat);

  print_info("get_stat_cache_entry : copie structure de %ld %p %p\n", real_ino,
             st, tmp);
  memcpy(st, tmp, sizeof(struct stat));
}

/**
 * Get the "open" cache entry corresponding to the named entry in a directory
 * which inode is given as `parent`.
 *
 * @param parent Inode of the folder in which we are looking.
 * @param name A string representing the name of the directory entry to be opened.
 *
 * @return FUSE inode for the searched parent + directory entry name. Can also be WRONG_PATH or GOOD_PATH.
 */
unsigned long get_cache_open(const ino_t parent, const char *name) {
  if (NO_CACHE)
    return 0;
  char *path = path_to_string(parent, name);

  pthread_mutex_lock(&mutex_open);
  long cache_entry = *((long *)(g_hash_table_lookup(cache_open, path)));
  pthread_mutex_unlock(&mutex_open);

  print_info("get_cache_open : search for entry %s : got %ld\n", path, cache_entry);
  free(path);
  return cache_entry;
}

// détruit les caches : TODO memory leaks
/**
 * The function "destroy_caches" is used to destroy caches.
 */
void destroy_caches() {
  if (NO_CACHE)
    return;
  g_hash_table_destroy(cache_open);
  g_hash_table_destroy(cache_stat);
}

/**
 * The function "print_open" is used to print the key-value pairs of a
 * GHashTable.
 *
 * @param key The key parameter is a pointer to the key of the current element
 * in the data structure being iterated over.
 * @param value The "value" parameter in the function "print_open" is a pointer
 * to the value associated with the key in the data structure being iterated
 * over.
 * @param user_data The user_data parameter is a pointer to any additional data
 * that you want to pass to the print_open function. It can be used to store any
 * custom data that you need within the function.
 */
void print_open(gpointer key, gpointer value, gpointer user_data) {
  if (NO_CACHE)
    return;
  printf("%s\t\t%ld\n", (char *)key, *((long *)value));
}

/**
 * The function "print_stat" is used to print the key-value pairs of a hash
 * table.
 *
 * @param key The key parameter is a pointer to the key of the current element
 * in the data structure being iterated over.
 * @param value The "value" parameter is a pointer to the value associated with
 * the key in the data structure being iterated over.
 * @param user_data The user_data parameter is a pointer to any additional data
 * that you want to pass to the print_stat function. It can be used to provide
 * context or additional information to the function.
 */
void print_stat(gpointer key, gpointer value, gpointer user_data) {
  if (NO_CACHE)
    return;
  printf("%ld\t\t stat \n", *((long *)key));
}

/**
 * The function "print_cache" is used to print the contents of a cache.
 */
void print_cache() {
  return;
  if (NO_CACHE)
    return;
  if (!DEBUG_CACHE_SYSTEM)
    return;
  printf("Cache OPEN : \n");
  pthread_mutex_lock(&mutex_open);
  g_hash_table_foreach(cache_open, print_open, NULL);
  pthread_mutex_unlock(&mutex_open);

  printf("\n\nCache STAT : \n");
  pthread_mutex_lock(&mutex_stat);
  g_hash_table_foreach(cache_stat, print_stat, NULL);
  pthread_mutex_unlock(&mutex_stat);
}

/**
 * The function updates the cache with the information of a newly opened file.
 *
 * @param parent A string representing the parent directory of the file.
 * @param name The name of the file or directory being updated in the cache.
 * @param ino The "ino" parameter represents the inode number of the file or
 * directory being updated in the cache.
 */
void update_cache_open(const ino_t parent, const char *name, long ino) {
  if (NO_CACHE)
    return;
  print_info("update_cache_open : Update open cache %u + %s -> %ld !\n", parent,
             name, ino);

  char *path = path_to_string(parent, name);

  pthread_mutex_lock(&mutex_open);
  g_hash_table_remove(cache_open, path); // remove cache entry
  long *value = (long *)calloc(1, sizeof(long));
  check_error_eq(value, "insert_cache", "calloc fail");
  *value = ino;
  g_hash_table_insert(cache_open, path, value);
  pthread_mutex_unlock(&mutex_open);
  free(path);
}

/**
 * The function updates or updates the file status cache for a given inode.
 *
 * @param real_ino The real_ino parameter is the inode number of the file or
 * directory for which we want to update the file status cache.
 * @param st A pointer to a `struct stat` object that is inserted in cache.
 */
void update_cache_stat(long real_ino, struct stat *st) {
  if (NO_CACHE)
    return;
  struct stat *sta = (struct stat *)calloc(1, sizeof(struct stat));
  check_error_eq(sta, "update_cache_stat", "calloc fail");
  long *key = (long *)calloc(1, sizeof(long));
  check_error_eq(key, "update_cache_stat", "calloc fail");

  *key = real_ino;

  memcpy(sta, st, sizeof(struct stat));

  pthread_mutex_lock(&mutex_stat);
  g_hash_table_insert(cache_stat, key, sta);
  statCount++;
  pthread_mutex_unlock(&mutex_stat);

  print_info("update_cache_stat : Ajout stat %d de %ld avec %p\n", statCount,
             real_ino, sta);
  // share_stat(real_ino,sta);
}

// TODO invalidate -> set a true
void invalide_cache_open(const char *path) {
  if (NO_CACHE)
    return;
  print_info("invalide_cache_open : Update (REMOVE) open cache %s !\n", path);
  pthread_mutex_lock(&mutex_open);
  g_hash_table_remove(cache_open, path);
  pthread_mutex_unlock(&mutex_open);
}

void invalide_cache_open_2(const ino_t parent, const char *name) {
  if (NO_CACHE)
    return;
  print_info("invalide_cache_open_2 : Update (REMOVE) open cache %s + %s !\n",
             parent, name);
  char *path = path_to_string(parent, name);
  pthread_mutex_lock(&mutex_open);
  g_hash_table_remove(cache_open, path); // remove la ligne
  pthread_mutex_unlock(&mutex_open);
  free(path);
}