# Search for paths that do not exist in test_mount and test_mount2.
# The paths differ in each search, so that each instance of chorage will log
# and share different cache entries.

ls test_mount1/lib1/v2/bin/lib1.txt
ls test_mount1/lib1/v2/bin1/lib.txt
ls test_mount1/lib1/v/bin/lib.txt
ls test_mount1/lib10/v2/bin/lib.txt

ls test_mount2/lib2/v1/bin/lib.txt
ls test_mount2/lib2/v1/bin1/lib.txt
ls test_mount2/lib2/v/bin/lib.txt
ls test_mount2/lib20/v2/bin/lib.txt

# Now, test a path on each that has been tried by the other, to confirm that
# the distribution of cache works.

ls test_mount1/lib2/v1/bin/lib.txt

ls test_mount2/lib1/v2/bin/lib1.txt