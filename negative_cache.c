#include "negative_cache.h"

#include <errno.h>
#include <glib.h>
#include <iostream>
#include <map>
#include <mutex>
#include <pthread.h>
#include <stdarg.h>
#include <stdio.h>
#include <unordered_map>

#ifndef DEBUG_CACHE_SYSTEM
#define DEBUG_CACHE_SYSTEM 0
#endif

/* DEBUG MANAGEMENT */
void print_info(const char *format, ...) {
  if (!DEBUG_CACHE_SYSTEM)
    return;
  va_list args;
  va_start(args, format);
  vprintf(format, args);
  va_end(args);
}

// Uniquely identifies a cache entry
typedef std::pair<ino_t, std::string> EntryId;

// Define a hash function for EntryId
namespace std {
template <> struct hash<EntryId> {
  size_t operator()(const EntryId &id) const {
    return hash<ino_t>{}(id.first) ^ hash<std::string>{}(id.second);
  }
};
} // namespace std

// Definition of the hash table, protected by negative_mutex
typedef std::unordered_map<EntryId, int> NegativeCache;
NegativeCache negative_cache;

// Mutex protecting NegativeCache
std::mutex negative_mutex;

/**
 * Record that a given name does not exist in a given parent directory.
 *
 * It involves the lock negative_mutex.
 *
 * @param parent Inode of the parent directory of the `name` entry. It is the
 *        Inode in the underlying file system which we assume to be the same
 *        for all participant in the shared cache.
 * @param name The name of the cache to be inserted.
 * @param The error number (errno) resulting from attempting to open `name` in
 * `parent` directory.
 */
void insert_negative_cache(const ino_t parent, std::string name, int error_nb) {
  print_info("insert_cache : parent %ld of name %s had failed with errno %d\n",
             parent, name.c_str(), error_nb);

  EntryId id{parent, name};

  std::unique_lock<std::mutex> cache_lock{negative_mutex};
  negative_cache.insert({id, error_nb});
  cache_lock.unlock();
}

/**
 * Check whether the given pair (parent, name) exits in the cache
 *
 * @param parent inode of the folder in which we are looking for
 * @param name name of the directory entry we are looking for
 *
 * @return 0 if absent, 1 otherwise
 */
bool in_negative_cache(const ino_t parent, std::string name) {
  print_info("in_negative_cache : searching for entry (%ld, %s) \n", parent,
             name.c_str());

  std::unique_lock<std::mutex> cache_lock{negative_mutex};
  auto in_cache_it = negative_cache.find({parent, name});
  cache_lock.unlock();
  return in_cache_it != negative_cache.end();
}

/**
 * Get the negative cache entry corresponding to the named entry in a directory
 * which inode is given as `parent`.
 *
 * @param parent Inode of the folder in which we are looking.
 * @param name A string representing the name of the directory entry to be
 *        opened.
 *
 * @return errno associated with the failed attempt to open name in parent
 *         or 0 if not in cache.
 */
int get_negative_cache(const ino_t parent, std::string name) {
  std::unique_lock<std::mutex> cache_lock{negative_mutex};
  auto cache_value = negative_cache.find({parent, name});
  cache_lock.unlock();

  if (cache_value != negative_cache.end()) {
    print_info("get_negative_cache : search for entry (%ld, %s) : got %ld\n",
               parent, name.c_str(), cache_value->second);
    return cache_value->second;
  } else {
    print_info("get_negative_cache : search for entry (%ld, %s) : not found\n",
               parent, name.c_str());
    return 0;
  }
}

/**
 * Print the contents of a cache.
 */
void print_negative_cache() {
  printf("Content of the negative cache : \n");
  std::unique_lock<std::mutex> cache_lock{negative_mutex};
  for (const auto &[key, value] : negative_cache)
    std::cout << "Key:[" << key.first << ", " << key.second << "] Value:[" << value << "]\n";
  cache_lock.unlock();
}