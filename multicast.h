#ifndef MULTICAST_H
#define MULTICAST_H

#include "settings.hpp"
#ifdef MULTICAST_LOG_ENABLE
#include "multicast_log.hpp"
#endif
#include <cstdio>
#include <errno.h>
#include <string>
#include <sys/stat.h> // for ino_t

void init_multicast_sockets(void (*insert_negative_cache)(const ino_t parent,
                                                          std::string name,
                                                          int error_nb) = NULL);
void send_message(const ino_t parent, const char *name, int error_number);
void receive_message();
void close_sockets();

#ifdef MULTICAST_LOG_ENABLE
extern MulticastLog multicast_log;
#endif

#endif // MULTICAST_H