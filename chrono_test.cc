#include <chrono>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <map>
#include <string>

int main() {
  std::map<int, int> hist, full_duration_hist;
  for (int n = 0; n != 10000; ++n) {
    auto start = std::chrono::high_resolution_clock::now();
    auto first_end = std::chrono::high_resolution_clock::now();
    auto second_end = std::chrono::high_resolution_clock::now();
    auto first_duration =
        std::chrono::duration_cast<std::chrono::nanoseconds>(first_end - start)
            .count();
    auto second_duration =
        std::chrono::duration_cast<std::chrono::nanoseconds>(second_end - start)
            .count();
    ++hist[std::round(first_duration)];
    ++full_duration_hist[std::round(second_duration)];
  }

  std::cout
      << "Distribution of duration between two consecutive calls to now():\n"
      << std::fixed << std::setprecision(1);
  for (auto [x, y] : hist)
    std::cout << std::setw(2) << x << ' ' << std::string(y / 200, '*') << '\n';

  std::cout << "Distribution of duration of time request\n";
  for (auto [x, y] : full_duration_hist)
    std::cout << std::setw(2) << x << ' ' << std::string(y / 200, '*') << '\n';

  const auto now = std::chrono::system_clock::now();
  const auto now_us = std::chrono::duration_cast<std::chrono::microseconds>(now.time_since_epoch());

  std::cout << "Current time: " << now_us.count() << " us\n";
}