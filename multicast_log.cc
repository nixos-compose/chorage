#include "multicast_log.hpp"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <string>
#include <unordered_map>

MulticastLog::MulticastLog() : receive_log() {}

void MulticastLog::increment(std::string source) { ++receive_log[source]; }

void MulticastLog::set_file_path(std::string file_path) {
  output_file.open(file_path, std::ios::out | std::ios::trunc);
}

bool MulticastLog::write_file() {
  if (!output_file.is_open())
    return false;

  output_file << "source, count" << std::endl;

  std::ostream_iterator<std::string> output_iterator(output_file, "\n");
  std::transform(
      std::begin(receive_log), std::end(receive_log), output_iterator,
      [](const auto entry) { return entry.first + "," + std::to_string(entry.second); });
  return true;
}

void MulticastLog::print_csv() {
  std::cout << "source, count" << std::endl;
  for (auto entry : receive_log) {
    std::cout << entry.first << "," << entry.second << std::endl;
  }
}