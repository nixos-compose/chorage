{
  description = "Chorage is a FUSE file system to transparently cache metadata operations on an underlying (read only) file system. When working in a cluster, it can multicast its cache to its peer instances of chorage.";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
  };

  outputs = { self, nixpkgs }: {

    packages.x86_64-linux.default =
      with import nixpkgs { system = "x86_64-linux"; };
      callPackage ./package.nix {};

    legacyPackages.x86_64-linux =  import nixpkgs { 
      system = "x86_64-linux"; 
      overlays = [
        (final: prev: {
          chorage = final.callPackage ./package.nix {};
        })
      ];
    };
  };
}
