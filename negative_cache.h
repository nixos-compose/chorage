#ifndef NEGATIVE_CACHE_H
#define NEGATIVE_CACHE_H

#include "chorage_inode.hpp"

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <string>

void insert_negative_cache(const ino_t parent, std::string name, int error_nb);
bool in_negative_cache(const ino_t parent, std::string name);
int get_negative_cache(const ino_t parent, std::string name);

void print_negative_cache();


#endif // NEGATIVE_CACHE_H